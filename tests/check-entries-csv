#!/usr/bin/env python3
# vim: set ts=8 sts=4 et sw=4 tw=99:
#
# Simple check script for entries.csv files.
# Used as part of "make check".
#
# Makes sure that:
#
# 1) All the column names are expected.
# 2) Each column's data are in expected formats for that column type.
#

import sys
import os

try:
    import oplcsv
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(
        os.path.dirname(os.path.realpath(__file__))), "scripts"))
    import oplcsv


def redden(s):
    if os.name != 'nt':
        return "\033[1;31m" + s + "\033[0;m"
    return s


def enyellow(s):
    if os.name != 'nt':
        return "\033[1;33m" + s + "\033[0;m"
    return s


globalFilename = ''
havePrintedFilename = False
errorCount = 0
warningCount = 0


def reset_filename_state(filename):
    global globalFilename
    global havePrintedFilename
    globalFilename = filename
    havePrintedFilename = False


def print_filename_once():
    # This completely breaks the main() abstraction, but whatever.
    global havePrintedFilename
    if not havePrintedFilename:
        print(globalFilename, file=sys.stderr)
        havePrintedFilename = True


def perror(s):
    global errorCount
    errorCount += 1
    print_filename_once()
    print(' ' + redden(s), file=sys.stderr)


def pwarning(s):
    global warningCount
    warningCount += 1
    print_filename_once()
    print(' ' + enyellow(s), file=sys.stderr)


def is_int(s):
    try:
        int(s)
        return s.isdigit()
    except ValueError:
        return False


def check_column_name(s, row, csv):
    if '  ' in s:
        pwarning("Too much spacing for name: \"%s\"" % s)

    # Lifters with the same name are disambiguated by tagging them
    # with an integer preceded by the '#' character.
    if '#' in s:
        if s.count('#') != 1:
            perror("Too many '#' characters in name '%s'." % s)
        parts = s.split('#')
        if len(parts) != 2 or not is_int(parts[1]):
            perror("Variant for '%s' must be a number." % s)

        # Conduct the rest of the check pretending that the variant
        # isn't present.
        s = parts[0].rstrip()

    # Disallow " in all circumstances: we already have to allow ',
    # so just use that for quotes.
    special_legal_chars = ' \'.-'

    if 'Jr.' in s or 'JR.' in s or 'Sr.' in s or 'SR.' in s:
        perror("Junior/Senior in names must standardize on no period: \"%s\"" % s)
    if 'Esq.' in s:
        perror("Esquire must be standardized on no period: \"%s\"" % s)

    # This only works for names written in English, but that's OK for now.
    has_illegal_char = False
    for c in s:
        if not c.isalpha() and c not in special_legal_chars:
            has_illegal_char = True
            break
    if has_illegal_char:
        perror("Name contains illegal character(s): \"%s\"" % s)

    part = s.split()
    if len(part) == 0:
        return

    # Need this check for Indonesian names given as just initials
    if any(len(x.replace('.', '')) > 1 for x in part):
        if s.upper() == s or s.lower() == s:
            perror("Name must be in proper case: \"%s\"" % s)
        elif not part[0][0].isupper():
            perror("Name must be in proper case: \"%s\"" % s)
        elif (not part[-1][0].isupper() and "d'" not in part[-1] and
                not part[-1].startswith('de')):
            perror("Name must be in proper case: \"%s\"" % s)

    if part[-1] == 'JR' or part[-1] == 'jr' or part[-1] == 'SR' or part[-1] == 'sr':
        perror("Junior/Senior in names must be capitalized like 'Jr': \"%s\"" % s)
    elif part[0] == 'Jr' or part[0] == 'Sr':  # USAPL does this.
        perror("Jr marking must be moved to end of name: \"%s\"" % s)

    # Some federations mark drug-testing using "-DT".
    if s.endswith('DT'):
        perror("Drug-testing information present in name: \"%s\"" % s)

    for p in part:
        if p[0] == "'" and p[-1] == "'":
            perror("Nicknames should be removed: \"%s\"" % s)

    if s[-4:] == ' Iii' or s[-3:] == ' Ii':
        pwarning("Fix name casing: \"%s\"" % s)


def check_row_name_consistency(row, csv):
    if 'Name' in csv.fieldnames:
        name = row[csv.index('Name')]
        if name is '':
            if 'JapaneseName' in csv.fieldnames:
                if row[csv.index('JapaneseName')] is '':
                    perror("JapaneseName field empty.")
            else:
                perror("Name field empty.")


def check_row(row, csv, meet_date, meet_id):
    check_row_name_consistency(row, csv)
    check_column_name(row[csv.index('Name')], row, csv)


def check(scriptname, filename, meetpath):
    # Pretty-printer helper (so we only print the name once).
    reset_filename_state(filename)

    csv = oplcsv.Csv(filename)

    meet_id = os.path.basename(os.path.dirname(filename))
    meet_date = None

    for row in csv.rows:
        if len(row) != len(csv.fieldnames):
            perror("Column count mismatch for row: %s" % ','.join(row))
            continue

        check_row(row, csv, meet_date, meet_id)


if __name__ == '__main__':
    # If arguments are provided, check only those files.
    if len(sys.argv) > 1:
        for f in sys.argv[1:]:
            # Need to think of a good syntax for passing config files
            check(sys.argv[0], f, None)

    # Otherwise, check every file named entries.csv in the dirtree,
    # which is much faster than using `find -exec`.
    else:
        for dirname, subdirs, files in os.walk(os.getcwd()):
            if "meet-data" in subdirs:
                subdirs[:] = ['meet-data']

            if 'entries.csv' in files:
                entriespath = dirname + os.sep + 'entries.csv'

                meetpath = None
                if 'meet.csv' in files:
                    meetpath = dirname + os.sep + 'meet.csv'

                check(sys.argv[0], entriespath, meetpath)

                if 'meet.csv' not in files:
                    perror('Missing meet.csv')

    print("Summary: %u errors, %u warnings." % (errorCount, warningCount))
    if errorCount > 0:
        sys.exit(1)
